---
title: Simultaneous Maze Solving
short_description: Solving several mazes with one solving sequence
---

![](simultaneous-maze-solving.png){:width="80%"}

## Description

[Simultaneous Maze Solving][1] is a game where we are given several grid mazes and we want to find a single sequence of moves (up, down, left, right) which solves each of them. This sequence should be as short as possible, or in another variant we might just be interested if a solving sequence of a certain length exists.

More formally, in this problem we are given a set of mazes $\mathcal{M} \subseteq \\{0,1\\}^{n \times m}$, where the $0$ fields are free and the $1$ fields are blocked, and we want to know if there exists a solving sequence $s \in \\{\text{up},\text{down},\text{left},\text{right}\\}^k$ of length $k$. We call a sequence $s$ of moves a solving sequence if for all mazes in $\mathcal{M}$, we visit the bottom right corner when starting from the top left corner when executing $s$. A sequence $s$ is executed such that we make the moves in each maze simultaneously, and we only stay on the unblocked (i.e., zero) fields of the maze. If we walk in direction of a blocked (i.e., one) field of a maze, we simply don't execute the move in this maze but possibly in the others.

## Computational Complexity 

There are two variants of this problem. One where we want to find a sequence of moves for a given set of mazes, and another one where we want to find a sequence for all solvable mazes of a certain size $n \times m$. The former problem is NP-complete while the latter problem is in PSPACE but no significant lower bound is known.

## Notes

A playable version of the NP-hardness reduction of [[1]] is available [here]({{site.baseurl}}/g/simultaneous-maze-solving/). 

## References

[[1]] Stefan Funke, André Nusser, and Sabine Storandt. "The Simultaneous Maze Solving Problem" AAAI Conference on Artificial Intelligence (2017)

[1]: https://aaai.org/ocs/index.php/AAAI/AAAI17/paper/view/14617
