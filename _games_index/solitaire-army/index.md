---
title: Solitaire Army
short_description: Given a region of a board and a target position inside that region, find a configuration of pegs outside the region and a sequence of moves that allows some peg to reach the target position.
---

![](solitairearmy.gif){:width="300px"}

## Description

Peg Solitaire Army problems are based on the game mechanics of *peg solitaire*
(see the corresponding [CoG page on Peg Solitaire]({{site.baseurl}}/i/peg-solitaire/)).

In a solitaire army problem, given a region of a -possibly infinite- board,
called *desert*, and a target position inside the desert, one is asked to find
a configuration of pegs outside the desert and a sequence of moves that allows
some peg to reach the target position.


## Results on Deserts and Target Positions 

In its classical formulation, introduced by J.H. Conway [[1]], the desert is a half-plane,
and the challenge is to understand what is the farthest distance in the desert that allows the
target position to be reached.
Conway devised an elegant potential argument to show that
distance $5$ cannot be reached on any finite board [[1]]. 

In [[1]], the authors investigate infinite desert shapes such as cones and quadrants of an infinite board, 
and finite desert shapes such as square-shaped and rhombus-shaped deserts in infinite boards, 
and corners of square boards. 
For square/rhombus deserts, they consider as natural target position the center
of the square/rhombus; their goal is that of finding the largest size of the desert for which
the puzzle is solvable. 
They show that the $9 \times 9$ square-shaped and $13 \times 13$ rhombus-shaped deserts are solvable, 
while the $13 \times 13$ square-shaped and $17 × 17$
rhombus-shaped are not. 
In [[2]], 
it was then shown that the $11 \times 11$ square-shaped and $15 \times 15$ rhombus-shaped
deserts are also solvable, among other variants of deserts.


## Notes

Solutions to various solitaire army problems given in [[2]], are playable online [here]({{site.baseurl}}/g/solitaire-army/). 


## References


[[1]] B. Csakany, R. Juhasz, “The Solitaire Army Reinspected,” Mathematics Magazine, 2000.

[[2]] L. Gualà, S. Leucci, E. Natale, R. Tauraso, “Large Peg-Army Maneuvers”, in FUN 2016.

[1]: https://www.jstor.org/stable/2690811
[2]: http://drops.dagstuhl.de/opus/volltexte/2016/5870/




